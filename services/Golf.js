import Api from '@/services/Api'

export default {
  getData (params, token) {
    return Api().get(`golf-clubs?${params}`, {
      headers: {
        Authorization: token
      }
    })
  },
  getOneClub (params, token) {
    return Api().get(`golf-clubs/${params}`, {
      headers: {
        Authorization: token
      }
    })
  },
  addGolf (golf, token) {
    const header = {
      headers: {
        Authorization: token
      }
    }
    return Api().post(`golf-clubs`, golf, header)
  },
  updateGolf (golfId, data, token) {
    const header = {
      headers: {
        Authorization: token
      }
    }
    return Api().patch(`golf-clubs/${golfId}`, data, header)
  }
}
