import Api from '@/services/Api'

export default {
  getInfo (userId, token) {
    return Api().get(`users/${userId}`, {
      headers: {
        Authorization: token
      }
    })
  },
  updateUser (userId, data, token) {
    const config = {
      headers: {
        Authorization: token
      }
    }
    return Api().patch(`users/${userId}`, data, config)
  }
}
