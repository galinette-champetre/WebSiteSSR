import axios from 'axios'

export default () => {
  return axios.create({
    baseURL: `https://galinette-golf-api.herokuapp.com/`
  })
}
