import Api from '@/services/Api'

export default {
  getIssue (params, token) {
    return Api().get(`issues?${params}`, {
      headers: {
        Authorization: token
      }
    })
  },
  getOneIssue (id, token) {
    return Api().get(`issues/${id}`, {
      headers: {
        Authorization: token
      }
    })
  },
  updateIssue (id, bool, token) {
    const header = {
      headers: {
        Authorization: token
      }
    }
    return Api().patch(`issues/${id}`, bool, header)
  }
}
