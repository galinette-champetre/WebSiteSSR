import Vuex from 'vuex'
import decode from 'jwt-decode'
// import gravatar from 'gravatar'
import UserService from '@/services/User'
import GolfService from '@/services/Golf'
import IssueService from '@/services/Issue'
import AuthenticationService from '@/services/Authentification'

const createStore = () => {
  return new Vuex.Store({
    state: {
      user: null,
      golfClubs: null,
      club: null,
      issues: null,
      oneIssue: null,
      token: null,
      // gravatar: '',
      isUserLoggedIn: false,
      error: false,
      sucess: false,
      loading: false
    },
    mutations: { // commit a muttation
      reinitState (state) {
        state.user = null
        state.golfClubs = null
        state.club = null
        state.issues = null
        state.oneIssue = null
        state.token = null
        state.isUserLoggedIn = false
        state.error = false
        state.sucess = false
        state.loading = false
      },
      setToken (state, token) {
        state.token = token
        if (token) {
          state.isUserLoggedIn = true
        } else {
          state.isUserLoggedIn = false
        }
      },
      setUser (state, user) {
        state.user = Object.assign({}, user)
      },
      setGravatar (state, gravatarUrl) {
        state.gravatar = gravatarUrl
      },
      setGolfClubs (state, golfList) {
        state.golfClubs = golfList
      },
      setClub (state, club) {
        state.club = club
      },
      setIssues (state, issues) {
        state.issues = issues
      },
      setOneIssue (state, issue) {
        state.oneIssue = issue
      },
      setLoading (state, bool) {
        state.loading = bool
      },
      setError (state, error) {
        state.error = error
      },
      setSuccess (state, success) {
        state.success = success
      }
    },
    getters: {
      getUser: state => state.user,
      isAuthenticated: state => state.isUserLoggedIn,
      getGravatar: state => state.gravatar,
      getGolfClubs: state => state.golfClubs,
      getClub: state => state.club,
      getIssues: state => state.issues,
      getOneIssue: state => state.oneIssue,
      getError: state => state.error,
      getSuccess: state => state.success,
      isLoading: state => state.loading
    },
    actions: { // dispatch an action
      async register ({commit}, data) {
        try {
          const response = await AuthenticationService.register({
            email: data.email,
            password: data.password,
            name: data.name,
            lastname: data.lastname
          })
          commit('setUser', response.data.user)
          commit('setError', null)
          commit('setSuccess', 'Compte créé avec succès. Veuillez vous identifier.')
        } catch (e) {
          commit('setSuccess', null)
          if (e.response.data.code === 409) {
            commit('setError', `L'email ${e.response.data.errors.email} existe déjà. Voulez-vous plutôt vous authentifier ?`)
          } else {
            commit('setError', e.response.data.message)
          }
        }
      },
      async login ({commit}, data) {
        try {
          commit('setLoading', true)
          const response = await AuthenticationService.login({
            strategy: 'local',
            email: data.email,
            password: data.password
          })
          const token = response.data.accessToken
          commit('setToken', token)
          commit('setLoading', false)
        } catch (e) {
          commit('setSuccess', null)
          if (e.response.data.code === 401) {
            commit('setError', 'Erreur dans le mail ou le mot de passe.')
          } else {
            commit('setError', e.response.data.message)
          }
          commit('setLoading', false)
        }
      },
      async updateUser ({commit, state}, newUserData) {
        commit('setLoading', true)
        const { token } = state
        const user = state.user
        try {
          const newUser = (await UserService.updateUser(user._id, newUserData, token)).data
          commit('setUser', newUser)
          commit('setLoading', false)
        } catch (e) {
          console.log(e)
          commit('setLoading', false)
        }
      },
      async getUserInfo ({commit, state, dispatch}) {
        const { token } = state
        const payload = decode(token)
        const { userId } = payload
        try {
          const { data } = await UserService.getInfo(userId, token)
          commit('setUser', data)
        } catch (e) {
          console.log(e)
        }

        // Get Club Data
        const { golfClubIds } = state.user
        if (golfClubIds.length > 0) {
          dispatch('getClubs', golfClubIds)
        }

        // Gravatar
        // const { email } = state.user
        // dispatch('getGravatar', email)
      },
      async getClubs ({commit, state, dispatch}, clubsId) {
        let params = ''
        clubsId.forEach(id => {
          params += `_id[$in]=${id}&`
        })

        try {
          const { token } = state
          const { data } = (await GolfService.getData(params, token)).data
          commit('setGolfClubs', data)
        } catch (e) {
          console.log(e)
        }

        // Get issue
        let issuesArray = []
        const { golfClubs } = state
        golfClubs.forEach(club => {
          const { issueIds } = club
          if (issueIds) {
            issueIds.forEach(id => issuesArray.push(id))
          }
        })
        if (issuesArray.length > 0) {
          dispatch('getIssues', issuesArray)
        }
      },
      async getIssues ({commit, state}, issuesId) {
        let params = ''
        issuesId.forEach(id => {
          params += `_id[$in]=${id}&`
        })

        try {
          const { token } = state
          const { data } = (await IssueService.getIssue(params, token)).data
          commit('setIssues', data)
        } catch (e) {
          console.log(e)
        }
      },
      async getClub ({commit, state}, clubId) {
        const { token } = state
        try {
          const data = (await GolfService.getOneClub(clubId, token)).data

          const EMPTY_DATA = {
            name: '',
            address: {
              club: {
                number: '',
                street: '',
                zip: '',
                city: '',
                country: '',
                phone: '',
                email: '',
                contactName: ''
              },
              bill: {
                number: '',
                street: '',
                zip: '',
                city: '',
                country: '',
                phone: '',
                email: ''
              }
            },
            contact: [],
            licence: {
              number: '',
              expired: ''
            },
            issueIds: []
          }

          const verifiedData = {
            _id: data._id,
            name: data.name || '',
            address: {
              club: {
                ...EMPTY_DATA.address.club, ...data.address.club
              },
              bill: {
                ...EMPTY_DATA.address.bill, ...data.address.bill
              }
            },
            contact: data.contact || [],
            licence: {
              ...EMPTY_DATA.licence, ...data.licence
            },
            issueIds: []
          }
          commit('setClub', verifiedData)
        } catch (error) {
          console.log(error)
        }
      },
      async addClub ({commit, state}, club) {
        commit('setLoading', true)
        const { token } = state
        try {
          const data = await GolfService.addGolf(club, token)
          this.$router.push({ name: 'golfclub-id', params: {id: data._id} })
        } catch (error) {
          console.log(error)
        }
        commit('setLoading', false)
      },
      async updateClub ({commit, state}, club) {
        commit('setLoading', true)
        const { token } = state

        try {
          const data = await GolfService.updateGolf(club._id, club, token)
          commit('setClub', data)
        } catch (error) {
          console.log(error)
        }
        commit('setLoading', false)
      },
      async getIssue ({commit, state}, issueId) {
        const { token } = state
        try {
          const { data } = await IssueService.getOneIssue(issueId, token)
          commit('setOneIssue', data)
        } catch (error) {
          console.log(error)
        }
      },
      // async getGravatar ({commit}, email) {
      //   try {
      //     const gravatarUrl = await gravatar.url(email, {protocol: 'https'})
      //     commit('setGravatar', `${gravatarUrl}?d=identicon`)
      //   } catch (e) {
      //     console.log(e)
      //   }
      // },
      toggleLoading ({commit, state}, bool) {
        commit('setLoading', bool)
      },
      async changeIssueDoneState ({commit, state}, issue) {
        const { token } = state
        const newData = {
          done: !issue.done
        }
        const { data } = await IssueService.updateIssue(issue._id, newData, token)
        commit('setOneIssue', data)
      }
    }
  })
}

export default createStore
