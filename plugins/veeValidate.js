import Vue from 'vue'
import VeeValidate from 'vee-validate'

const config = {
  delay: 250,
  classNames: {
    valid: 'is-success', // model is valid
    invalid: 'is-danger' // model is invalid
  }
}

Vue.use(VeeValidate, config)
